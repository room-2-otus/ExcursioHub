using System.Collections.Generic;
using System.Threading.Tasks;
using ExcursioHub.Shared.DTOs;
using ExcursioHub.Shared.Entities;
using ExcursioHub.Shared.Models;

namespace ExcursioHub.Client.Contracts
{
    public interface IProductService
    {
        Task<PagedList<ProductDTO>> GetProductsAsync(ProductParameters productParameters, bool IsAdmin = false);
        Task<ProductDTO> GetProductByIdAsync(int id);
        Task<ProductDTO> GetProductByProductIdAndLanguagIdAsync(int productId, int languageId);
        Task<Category> GetCategoryByIdAsync(int id);
        Task<Guide> GetGuideByIdAsync(int id);
        Task<List<ProductDTO>> GetProductsByGuideAsync(int guideId);
        Task<List<ProductDTO>> GetProductsByCategoryAsync(int categoryId);
        Task<List<Guide>> GetProductGuidesAsync();
        Task<List<Category>> GetProductCategoriesAsync(int languageId = 0);
        Task<PagedList<ProductDTO>> GetProductsOnSpecialOfferAsync(ProductParameters productParameters);
        Task<HttpResponseMessage> AddProductAsync(ProductCreateDTO productDTO);
        Task<HttpResponseMessage> UpdateProductAsync(int id, ProductUpdateDTO productDTO);
        Task<HttpResponseMessage> DeleteProductAsync(int id);
        Task<HttpResponseMessage> AddGuideAsync(GuideDTO guide);
        Task<HttpResponseMessage> UpdateGuideAsync(int id, GuideDTO guide);
        Task<HttpResponseMessage> DeleteGuideAsync(int id);
        Task<HttpResponseMessage> AddCategoryAsync(Category category);
        Task<HttpResponseMessage> UpdateCategoryAsync(int id, Category category);
        Task<HttpResponseMessage> DeleteCategoryAsync(int id);
    }
}