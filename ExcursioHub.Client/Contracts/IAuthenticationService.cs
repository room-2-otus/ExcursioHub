using System.Threading.Tasks;
using ExcursioHub.Shared.Auth;

namespace ExcursioHub.Client.Contracts
{
    public interface IAuthenticationService
    {
        Task<LoginResult> Login(LoginModel loginModel);
        Task Logout();
        Task<RegisterResult> Register(RegisterModel registerModel);
    }
}