﻿using ExcursioHub.Shared.Models;

namespace ExcursioHub.Client.Contracts
{
    public interface ILanguageService
    {
        Task<Language> GetLanguageByLanguageIdAsync(String languageId);
        Task<List<Language>> GetAllLanguagesAsync();
    }
}
