using System.Threading.Tasks;
using ExcursioHub.Shared.DTOs;
using ExcursioHub.Shared.Models.Identity;

namespace ExcursioHub.Client.Contracts
{
    public interface IAccountService
    {
        Task<AddressDTO> GetUserAddressAsync();
        Task<AddressDTO> SaveUserAddressAsync(AddressDTO address);
    }
}