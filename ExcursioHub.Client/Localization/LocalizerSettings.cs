﻿namespace ExcursioHub.Client.Localization
{
    public static class LocalizerSettings
    {
        public static CultureWithName NeutralCulture = new CultureWithName("English", "en-US");

        public static readonly List<CultureWithName> SupportedCulturesWithName =
            new List<CultureWithName>()
            {
                new CultureWithName("EN", "en-US"),
                new CultureWithName("RU", "ru-RU"),
                new CultureWithName("ET", "et-EE")
            };
    }
}
