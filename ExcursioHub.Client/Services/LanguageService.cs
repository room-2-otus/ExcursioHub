﻿using Blazored.LocalStorage;
using ExcursioHub.Client.Contracts;
using ExcursioHub.Shared.DTOs;
using ExcursioHub.Shared.Models;
using FluentValidation.Resources;
using System.Net.Http.Json;

namespace ExcursioHub.Client.Services
{
    public class LanguageService : ILanguageService
    {
        private readonly IHttpClientFactory _httpClient;

        public LanguageService(IHttpClientFactory httpClient, ILocalStorageService localStorage)
        {
            _httpClient = httpClient;
        }

        public async Task<Language> GetLanguageByLanguageIdAsync(string languageId)
        {
            try
            {
                var client = _httpClient.CreateClient("ExcursioHubAPI");
                var language = await client.GetFromJsonAsync<Language>($"languages/lang/{languageId}");

                return language;
            }
            catch (HttpRequestException ex)
            {
                throw new HttpRequestException(ex.Message, ex.InnerException, ex.StatusCode);
            }
        }
        
        public async Task<List<Language>> GetAllLanguagesAsync()
        {
            try
            {
                var client = _httpClient.CreateClient("ExcursioHubAPI");
                var language = await client.GetFromJsonAsync<List<Language>>($"languages/");

                return language;
            }
            catch (HttpRequestException ex)
            {
                throw new HttpRequestException(ex.Message, ex.InnerException, ex.StatusCode);
            }
        }
    }
}
