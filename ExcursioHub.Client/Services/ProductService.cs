using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading.Tasks;
using Blazored.LocalStorage;
using ExcursioHub.Client.Contracts;
using ExcursioHub.Client.Pages.Admin;
using ExcursioHub.Shared.DTOs;
using ExcursioHub.Shared.Entities;
using ExcursioHub.Shared.Models;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using Microsoft.AspNetCore.WebUtilities;
using Stripe;

namespace ExcursioHub.Client.Services
{
    public class ProductService : IProductService
    {
        private readonly IHttpClientFactory _httpClient;
        private readonly ILocalStorageService _localStorage;

        public ProductService(IHttpClientFactory httpClient, ILocalStorageService localStorage)
        {
            _localStorage = localStorage;
            _httpClient = httpClient;
        }

        public async Task<ProductDTO> GetProductByIdAsync(int id)
        {
            try
            {
                var client = _httpClient.CreateClient("ExcursioHubAPI");
                var product = await client.GetFromJsonAsync<ProductDTO>($"products/{id}");

                return product;
            }
            catch (HttpRequestException ex)
            {
                throw new HttpRequestException(ex.Message);
            }
        }
        
        public async Task<ProductDTO> GetProductByProductIdAndLanguagIdAsync(int productId, int languageId)
        {
            try
            {
                var client = _httpClient.CreateClient("ExcursioHubAPI");
                var product = await client.GetFromJsonAsync<ProductDTO>($"products/{productId}/{languageId}");

                return product;
            }
            catch (HttpRequestException ex)
            {
                throw new HttpRequestException(ex.Message);
            }
        }
        

        public async Task<PagedList<ProductDTO>> GetProductsAsync(ProductParameters productParams, bool IsAdmin = false)
        {
            try
            {
                var request = new HttpRequestMessage();

                if (productParams != null) 
                {
                    var queryStringParams = new Dictionary<string, string>
                    {
                        ["pageIndex"] = productParams.PageIndex < 1  ? "1" : productParams.PageIndex.ToString(),
                    };

                    // conditionally add a page size param (number of items to return)
                    if (productParams.PageSize != 0)
                    {
                        queryStringParams.Add("pageSize", productParams.PageSize.ToString());
                    };

                    // conditionally add a search term
                    if (!String.IsNullOrEmpty(productParams.Search))
                    {
                        queryStringParams.Add("search", productParams.Search.ToString());
                    };

                    // conditionally add a categoryId param
                    if (productParams.CategoryId != 0)
                    {
                        queryStringParams.Add("categoryId", productParams.CategoryId.ToString());
                    };

                    // conditionally add a guideId param
                    if (productParams.GuideId != 0)
                    {
                        queryStringParams.Add("guideId", productParams.GuideId.ToString());
                    };

                    if (productParams.LanguageId != 0)
                    {
                        queryStringParams.Add("languageId", productParams.LanguageId.ToString());
                    }

                    // conditionally add a sort param
                    if (!String.IsNullOrEmpty(productParams.SortBy))
                    {
                        queryStringParams.Add("sort", productParams.SortBy.ToString());
                    };

                    if (IsAdmin)
                    {
                        queryStringParams.Add("IsAdmin", Boolean.TrueString);
                    }



                    request = new HttpRequestMessage(HttpMethod.Get, QueryHelpers.AddQueryString("products", queryStringParams));
                }
                else
                {
                    request = new HttpRequestMessage(HttpMethod.Get, "products");
                }

                var client = _httpClient.CreateClient("ExcursioHubAPI");
                HttpResponseMessage response = await client.SendAsync(request);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var content = await response.Content.ReadAsStringAsync();

                    var pagedResponse = new PagedList<ProductDTO>
                    {
                        Items = JsonSerializer.Deserialize<List<ProductDTO>>(
                            content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true }
                        ),
                        Metadata = JsonSerializer.Deserialize<PagingMetadata>(
                            response.Headers.GetValues("Pagination")
                            .First(), new JsonSerializerOptions { PropertyNameCaseInsensitive = true }
                        )
                    };

                    return pagedResponse;
                }

                return null;
            }
            catch (HttpRequestException ex)
            {
                throw new HttpRequestException(ex.Message, ex.InnerException, ex.StatusCode);
            }
        }

        public async Task<List<ProductDTO>> GetProductsByGuideAsync(int guideId)
        {
            try
            {
                var client = _httpClient.CreateClient("ExcursioHubAPI");
                var products = await client.GetFromJsonAsync<List<ProductDTO>>($"products?guideId={guideId}");

                return products;
            }
            catch (HttpRequestException ex)
            {
                throw new HttpRequestException(ex.Message, ex.InnerException, ex.StatusCode);
            }
        }

        public async Task<List<ProductDTO>> GetProductsByCategoryAsync(int categoryId)
        {
            try
            {
                var client = _httpClient.CreateClient("ExcursioHubAPI");
                var products = await client.GetFromJsonAsync<List<ProductDTO>>($"products?categoryId={categoryId}");

                return products;
            }
            catch (HttpRequestException ex)
            {
                throw new HttpRequestException(ex.Message, ex.InnerException, ex.StatusCode);
            }
        }

        public async Task<List<Guide>> GetProductGuidesAsync()
        {
            try
            {
                var client = _httpClient.CreateClient("ExcursioHubAPI");
                var guides = await client.GetFromJsonAsync<List<Guide>>("guides");

                return guides;
            }
            catch (HttpRequestException ex)
            {
                throw new HttpRequestException(ex.Message, ex.InnerException, ex.StatusCode);
            }
        }
        public async Task<List<Category>> GetProductCategoriesAsync(int languageId) //TODO: get languageId for the choosen language
        {
            try
            {

                var client = _httpClient.CreateClient("ExcursioHubAPI");
                var categories = await client.GetFromJsonAsync<List<Category>>($"categories/lang/{languageId}");

                return categories;
            }
            catch (HttpRequestException ex)
            {
                throw new HttpRequestException(ex.Message, ex.InnerException, ex.StatusCode);
            }
        }

        public async Task<PagedList<ProductDTO>> GetProductsOnSpecialOfferAsync(ProductParameters productParams)
        {
            try
            {
                var request = new HttpRequestMessage();

                if (productParams != null) 
                {
                    var queryStringParams = new Dictionary<string, string>
                    {
                        ["pageIndex"] = productParams.PageIndex < 1  ? "1" : productParams.PageIndex.ToString(),
                    };

                    // conditionally add a page size param (number of items to return)
                    if (productParams.PageSize != 0)
                    {
                        queryStringParams.Add("pageSize", productParams.PageSize.ToString());
                    };

                    // conditionally add a search term
                    if (!String.IsNullOrEmpty(productParams.Search))
                    {
                        queryStringParams.Add("search", productParams.Search.ToString());
                    };

                    // conditionally add a categoryId param
                    if (productParams.CategoryId != 0)
                    {
                        queryStringParams.Add("categoryId", productParams.CategoryId.ToString());
                    };

                    // conditionally add a guideId param
                    if (productParams.GuideId != 0)
                    {
                        queryStringParams.Add("guideId", productParams.GuideId.ToString());
                    };

                    if (productParams.LanguageId != 0)
                    {
                        queryStringParams.Add("languageId", productParams.LanguageId.ToString());
                    }

                    // conditionally add a sort param
                    if (!String.IsNullOrEmpty(productParams.SortBy))
                    {
                        queryStringParams.Add("sort", productParams.SortBy.ToString());
                    };

                    

                    request = new HttpRequestMessage(HttpMethod.Get, QueryHelpers.AddQueryString("offers", queryStringParams));
                }
                else
                {
                    request = new HttpRequestMessage(HttpMethod.Get, "offers");
                }

                var client = _httpClient.CreateClient("ExcursioHubAPI");
                HttpResponseMessage response = await client.SendAsync(request);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var content = await response.Content.ReadAsStringAsync();

                    var pagedResponse = new PagedList<ProductDTO>
                    {
                        Items = JsonSerializer.Deserialize<List<ProductDTO>>(
                            content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true }
                        ),
                        Metadata = JsonSerializer.Deserialize<PagingMetadata>(
                            response.Headers.GetValues("Pagination")
                            .First(), new JsonSerializerOptions { PropertyNameCaseInsensitive = true }
                        )
                    };

                    return pagedResponse;
                }

                return null;
            }
            catch (HttpRequestException ex)
            {
                throw new HttpRequestException(ex.Message, ex.InnerException, ex.StatusCode);
            }
        }



        public async Task<HttpResponseMessage> AddProductAsync(ProductCreateDTO productDTO)
        { 
            try
            {

                var storedToken = await _localStorage.GetItemAsync<string>("authToken");

                var client = _httpClient.CreateClient("ExcursioHubAPI");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", storedToken);
                var product = await client.PostAsJsonAsync<ProductCreateDTO>($"products", productDTO);

                return product;
            }
            catch (HttpRequestException ex)
            {
                throw new HttpRequestException(ex.Message);
            }
        }

        public async Task<HttpResponseMessage> UpdateProductAsync(int id, ProductUpdateDTO productDTO)
        {

            try
            {
                var storedToken = await _localStorage.GetItemAsync<string>("authToken");

                var client = _httpClient.CreateClient("ExcursioHubAPI");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", storedToken);
                var product = await client.PutAsJsonAsync<ProductUpdateDTO>($"products/{id}" , productDTO);
                
                return product;
            }
            catch (HttpRequestException ex)
            {
                throw new HttpRequestException(ex.Message);
            }
        }

        public async Task<HttpResponseMessage> DeleteProductAsync(int productId)
        {
            try
            {
                var storedToken = await _localStorage.GetItemAsync<string>("authToken");

                var client = _httpClient.CreateClient("ExcursioHubAPI");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", storedToken);
                var product = await client.DeleteAsync($"products/{productId}");

                return product;
            }
            catch (HttpRequestException ex)
            {
                throw new HttpRequestException(ex.Message);
            }
        }

        public async Task<Guide> GetGuideByIdAsync(int id)
        {
            try
            {
                var client = _httpClient.CreateClient("ExcursioHubAPI");
                var guide = await client.GetFromJsonAsync<Guide>($"guides/{id}");

                return guide;
            }
            catch (HttpRequestException ex)
            {
                throw new HttpRequestException(ex.Message);
            }
        }

        public async Task<HttpResponseMessage> AddGuideAsync(GuideDTO guide)
        {
            try
            {

                var storedToken = await _localStorage.GetItemAsync<string>("authToken");

                var client = _httpClient.CreateClient("ExcursioHubAPI");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", storedToken);
                var product = await client.PostAsJsonAsync<GuideDTO>($"guides", guide);

                return product;
            }
            catch (HttpRequestException ex)
            {
                throw new HttpRequestException(ex.Message);
            }
        }
        public async Task<HttpResponseMessage> UpdateGuideAsync(int id, GuideDTO guide)
        {
            try
            {
                var storedToken = await _localStorage.GetItemAsync<string>("authToken");

                var client = _httpClient.CreateClient("ExcursioHubAPI");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", storedToken);
                var product = await client.PutAsJsonAsync<GuideDTO>($"guides/{id}", guide);

                return product;
            }
            catch (HttpRequestException ex)
            {
                throw new HttpRequestException(ex.Message);
            }
        }

        public async Task<HttpResponseMessage> DeleteGuideAsync(int id)
        {
            try
            {
                var storedToken = await _localStorage.GetItemAsync<string>("authToken");

                var client = _httpClient.CreateClient("ExcursioHubAPI");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", storedToken);
                var product = await client.DeleteAsync($"guides/{id}");

                return product;
            }
            catch (HttpRequestException ex)
            {
                throw new HttpRequestException(ex.Message);
            }
        }

        public async Task<Category> GetCategoryByIdAsync(int id)
        {
            try
            {
                var client = _httpClient.CreateClient("ExcursioHubAPI");
                var category = await client.GetFromJsonAsync<Category>($"categories/{id}");

                return category;
            }
            catch (HttpRequestException ex)
            {
                throw new HttpRequestException(ex.Message);
            }
        }

        public async Task<HttpResponseMessage> AddCategoryAsync(Category category)
        {
            try
            {

                var storedToken = await _localStorage.GetItemAsync<string>("authToken");

                var client = _httpClient.CreateClient("ExcursioHubAPI");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", storedToken);
                var cat = await client.PostAsJsonAsync<Category>($"categories", category);

                return cat;
            }
            catch (HttpRequestException ex)
            {
                throw new HttpRequestException(ex.Message);
            }
        }

        public async Task<HttpResponseMessage> UpdateCategoryAsync(int id, Category category)
        {
            try
            {
                var storedToken = await _localStorage.GetItemAsync<string>("authToken");

                var client = _httpClient.CreateClient("ExcursioHubAPI");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", storedToken);
                var cat = await client.PutAsJsonAsync<Category>($"categories/{id}", category);

                return cat;
            }
            catch (HttpRequestException ex)
            {
                throw new HttpRequestException(ex.Message);
            }
        }

        public async Task<HttpResponseMessage> DeleteCategoryAsync(int id)
        {
            try
            {
                var storedToken = await _localStorage.GetItemAsync<string>("authToken");

                var client = _httpClient.CreateClient("ExcursioHubAPI");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", storedToken);
                var category = await client.DeleteAsync($"categories/{id}");

                return category;
            }
            catch (HttpRequestException ex)
            {
                throw new HttpRequestException(ex.Message);
            }
        }
    }
}