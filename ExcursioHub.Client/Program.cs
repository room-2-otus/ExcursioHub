using Blazored.LocalStorage;
using Blazored.SessionStorage;
using Blazored.Toast;
using ExcursioHub.Client.Contracts;
using ExcursioHub.Client.Providers;
using ExcursioHub.Client.Services;
using ExcursioHub.Shared.Auth;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Polly;
using Serilog;
using Polly.Extensions.Http;
using ExcursioHub.Client;
using Microsoft.AspNetCore.Components.Web;
using System.Globalization;
using ExcursioHub.Client.Extensions;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.Services.AddLocalization(options =>
{
    options.ResourcesPath = "Resources";
});

builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services.AddHttpClient("IdentityAPI", c => c.BaseAddress =
    new Uri(builder.Configuration["ApiSettings:IdentityUri"] + "/api/"))
    .AddPolicyHandler(GetRetryPolicy())
    .AddPolicyHandler(GetCircuitBreakerPolicy());



builder.Services.AddHttpClient("ExcursioHubAPI", c => c.BaseAddress = 
    new Uri(builder.HostEnvironment.BaseAddress + "api/"))
    .AddPolicyHandler(GetRetryPolicy())
    .AddPolicyHandler(GetCircuitBreakerPolicy());

builder.Services.AddHttpClient("MontonioAPI", c => c.BaseAddress =
    new Uri("https://localhost:61611/api/"))
    .AddPolicyHandler(GetRetryPolicy())
    .AddPolicyHandler(GetCircuitBreakerPolicy());

builder.Services.AddScoped<IProductService, ProductService>();
builder.Services.AddScoped<IOrderService, OrderService>();
builder.Services.AddScoped<IPaymentService, PaymentService>();
builder.Services.AddScoped<IAccountService, AccountService>();
builder.Services.AddScoped<ILanguageService, LanguageService>();
            
builder.Services.AddApiAuthorization(opt => opt.UserOptions.RoleClaim = "role");
builder.Services.AddAuthorizationCore(config =>
{
    config.AddPolicy("IsAdmin", policyBuilder =>
    {
        policyBuilder.RequireClaim("role", "Administrator");

    });

    config.AddPolicy("IsUser", policyBuilder =>
    {
        policyBuilder.RequireClaim("role", "User");

    });
});

builder.Services.AddScoped<AuthenticationStateProvider, ApiAuthenticationStateProvider>();
builder.Services.AddScoped<IAuthenticationService, AuthenticationService>();



builder.Services.AddBlazoredSessionStorage();
builder.Services.AddBlazoredToast();
builder.Services.AddBlazoredLocalStorage();


WebAssemblyHost host = builder.Build();
await host.SetDefaultCulture();
await host.RunAsync();
        

IAsyncPolicy<HttpResponseMessage> GetRetryPolicy()
{
    // Retry with jitter: https://github.com/App-vNext/Polly/wiki/Retry-with-jitter
    Random jitter = new Random();

    return HttpPolicyExtensions
        .HandleTransientHttpError()
        .WaitAndRetryAsync(
            retryCount: 5,
            sleepDurationProvider: retryAttempt =>
                    TimeSpan.FromSeconds(Math.Pow(2, retryAttempt))  // exponential backoff (2, 4, 8, 16, 32 secs)
                + TimeSpan.FromMilliseconds(jitter.Next(0, 1000)), // plus some jitter: up to 1 second
            onRetry: (exception, retryCount, context) =>
            {
                Log.Warning("Retry {retryCount} of {policyKey} at {operationKey}, due to: {exception}.",
                    retryCount, context.PolicyKey, context.OperationKey, exception);
            });
}

IAsyncPolicy<HttpResponseMessage> GetCircuitBreakerPolicy()
{
    return HttpPolicyExtensions
        .HandleTransientHttpError()
        .CircuitBreakerAsync(
            handledEventsAllowedBeforeBreaking: 5,
            durationOfBreak: TimeSpan.FromSeconds(30)
        );
}
