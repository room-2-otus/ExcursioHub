using ExcursioHub.Server.Helpers;
using ExcursioHub.Shared.Models;

namespace ExcursioHub.Server.Specifications
{
    public class ProductsWithGuidesAndCategoriesSpecification : BaseSpecification<Product>
    {
        public ProductsWithGuidesAndCategoriesSpecification(ProductSpecificationParams productParams)
            : base(p =>
                (string.IsNullOrEmpty(productParams.Search) || p.Name.ToLower().Contains(productParams.Search)) &&
                (!productParams.GuideId.HasValue || p.GuideId == productParams.GuideId) &&
                (!productParams.CategoryId.HasValue || p.CategoryId == productParams.CategoryId) &&
                (!productParams.LanguageId.HasValue || p.LanguageId == productParams.LanguageId) &&
                p.IsAvailable == true 
            )
        {
            // Includes
            AddInclude(p => p.Guide);
            AddInclude(p => p.Category);

            // Paging
            ApplyPaging(productParams.PageSize * (productParams.PageIndex - 1), productParams.PageSize);

            // Sorting
            if (!string.IsNullOrEmpty(productParams.Sort))
            {
                switch (productParams.Sort)
                {
                    case "nameAsc":
                        ApplyOrderBy(p => p.Name);
                        break;
                    case "nameDesc":
                        ApplyOrderByDescending(p => p.Name);
                        break;
                    case "guideAsc":
                        ApplyOrderBy(p => p.Guide.Name);
                        break;
                    case "guideDesc":
                        ApplyOrderByDescending(p => p.Guide.Name);
                        break;
                    case "priceAsc":
                        ApplyOrderBy(p => p.SalePrice != 0 && p.SalePrice < p.Price ? p.SalePrice : p.Price);
                        break;
                    case "priceDesc":
                        ApplyOrderByDescending(p => p.SalePrice != 0 && p.SalePrice < p.Price ? p.SalePrice : p.Price);
                        break;
                    case "idAsc":
                        ApplyOrderBy(p => p.Id);
                        break;
                    case "idDesc":
                        ApplyOrderByDescending(p => p.Id);
                        break;
                    default:
                        ApplyOrderBy(p => p.Name);
                        break;
                }
            }
            else
            {
                ApplyOrderBy(p => p.Name);
            }
        }

        public ProductsWithGuidesAndCategoriesSpecification(int id) : base(p => p.Id == id)
        {
            AddInclude(p => p.Guide);
            AddInclude(p => p.Category);
        }
        public ProductsWithGuidesAndCategoriesSpecification(int productId, int languageId) : base(p => p.ProductId == productId && p.LanguageId == languageId)
        {
            AddInclude(p => p.Guide);
            AddInclude(p => p.Category);
        }
    }
}