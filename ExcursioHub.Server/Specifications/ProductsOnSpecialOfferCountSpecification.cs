using ExcursioHub.Server.Helpers;
using ExcursioHub.Shared.Models;

namespace ExcursioHub.Server.Specifications
{
    public class ProductsOnSpecialOfferCountSpecification : BaseSpecification<Product>
    {
        public ProductsOnSpecialOfferCountSpecification(ProductSpecificationParams productParams)
            : base(p =>
                (string.IsNullOrEmpty(productParams.Search) || p.Name.ToLower().Contains(productParams.Search)) &&
                (!productParams.GuideId.HasValue || p.GuideId == productParams.GuideId) &&
                (!productParams.CategoryId.HasValue || p.CategoryId == productParams.CategoryId) &&
                (!productParams.LanguageId.HasValue || p.LanguageId == productParams.LanguageId) &&
                (p.SalePrice != 0 && p.SalePrice < p.Price) &&
                p.IsAvailable == true
            )
        {
        }
    }
}