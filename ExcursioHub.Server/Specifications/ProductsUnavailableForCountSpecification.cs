﻿using ExcursioHub.Server.Helpers;
using ExcursioHub.Shared.Models;

namespace ExcursioHub.Server.Specifications
{
    public class ProductsUnavailableForCountSpecification : BaseSpecification<Product>
    {
        public ProductsUnavailableForCountSpecification(ProductSpecificationParams productParams)
            : base(p =>
                (string.IsNullOrEmpty(productParams.Search) || p.Name.ToLower().Contains(productParams.Search)) &&
                (!productParams.GuideId.HasValue || p.GuideId == productParams.GuideId) &&
                (!productParams.CategoryId.HasValue || p.CategoryId == productParams.CategoryId) &&
                p.IsAvailable == false
            )
        {
        }
    }
}
