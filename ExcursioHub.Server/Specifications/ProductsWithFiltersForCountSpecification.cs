﻿using ExcursioHub.Server.Helpers;
using ExcursioHub.Shared.Models;

namespace ExcursioHub.Server.Specifications
{
    public class ProductsWithFiltersForCountSpecification : BaseSpecification<Product>
    {
        public ProductsWithFiltersForCountSpecification(ProductSpecificationParams productParams) 
            : base(p =>
                (string.IsNullOrEmpty(productParams.Search) || p.Name.ToLower().Contains(productParams.Search)) &&
                (!productParams.GuideId.HasValue || p.GuideId == productParams.GuideId) &&
                (!productParams.CategoryId.HasValue || p.CategoryId == productParams.CategoryId) &&
                (!productParams.LanguageId.HasValue || p.LanguageId == productParams.LanguageId) &&
                (p.IsAvailable == true || productParams.IsAdmin == true)
            )
        {
        }
    }
}
