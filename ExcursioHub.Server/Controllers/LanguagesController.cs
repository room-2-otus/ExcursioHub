﻿using ExcursioHub.Server.Contracts;
using ExcursioHub.Shared.Models;
using Microsoft.AspNetCore.Mvc;

namespace ExcursioHub.Server.Controllers
{
    public class LanguagesController : BaseApiController
    {

        private readonly ILogger<LanguagesController> _logger;
        private readonly ILanguageRepository _languageRepository;
        
        public LanguagesController(ILogger<LanguagesController> logger, ILanguageRepository languageRepository)
        {
            _logger = logger;
            _languageRepository = languageRepository;
        }

        // GET api/languages
        [HttpGet]
        [ResponseCache(Duration = 300)]
        public async Task<ActionResult<IEnumerable<Language>>> GetLanguages()
        {
            try
            {
                var languages = await _languageRepository.ListAllAsync();

                return Ok(languages);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error in GetLanguages : {ex.Message}");

                return StatusCode(500, "Internal server error");
            }
        }

        // GET api/languages/{id}
        [HttpGet("{id}")]
        [ResponseCache(Duration = 300, VaryByQueryKeys = new[] { "id" })]
        public async Task<ActionResult<Language>> GetLanguageById(int id)
        {
            try
            {
                var language = await _languageRepository.GetByIdAsync(id);

                if (language == null)
                {
                    _logger.LogError($"Language with id: {id}, not found");

                    return NotFound();
                }
                else
                {
                    return Ok(language);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error in GetLanguageById : {ex.Message}");

                return StatusCode(500, "Internal server error");
            }
        }

        // GET api/languages/lang/{languageId}
        [HttpGet("lang/{languageId}")]
        [ResponseCache(Duration = 300, VaryByQueryKeys = new[] { "languageId" })]
        public async Task<ActionResult<Language>> GetLanguageByLanguageId(string languageId)
        {
            try
            {
                var language = await _languageRepository.GetLanguageByLanguageIdAsync(languageId);

                if (language == null)
                {
                    _logger.LogError($"Language with languageId: {languageId}, not found");

                    return NotFound();
                }
                else
                {
                    return Ok(language);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error in GetLanguageByLanguageId : {ex.Message}");

                return StatusCode(500, "Internal server error");
            }
        }


    }
}
