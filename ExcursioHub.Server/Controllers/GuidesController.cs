using ExcursioHub.Server.Contracts;
using ExcursioHub.Shared.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using ExcursioHub.Shared.DTOs;
using MassTransit;
using Contracts;

namespace ExcursioHub.Server.Controllers
{
    public class GuidesController : BaseApiController
    {
        private readonly ILogger<GuidesController> _logger;
        private readonly IGuideRepository _guideRepository;
        private readonly IPublishEndpoint _publishEndpoint;


        public GuidesController(ILogger<GuidesController> logger, IGuideRepository guideRepository, IPublishEndpoint publishEndpoint)
        {
            _logger = logger;
            _guideRepository = guideRepository;
            _publishEndpoint = publishEndpoint;
        }

        // GET api/guides
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Guide>>> GetGuides()
        {
            try
            {
                var guides = await _guideRepository.ListAllAsync();

                return Ok(guides);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error in GetGuides : {ex.Message}");

                return StatusCode(500, "Internal server error");
            }
        }

        // GET api/guides/{id}
        [HttpGet("{id}")]
        public async Task<ActionResult> GetGuideById(int id)
        {
            try
            {
                var guide = await _guideRepository.GetByIdAsync(id);

                if (guide == null)
                {
                    _logger.LogError($"Guide with id: {id}, not found");

                    return NotFound();
                }
                else
                {
                    return Ok(guide);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error in GetGuideById : {ex.Message}");

                return StatusCode(500, "Internal server error");
            }
        }


        // POST api/guides
        /// <summary>
        /// Creates a new guide
        /// </summary>
        /// <returns>Guide</returns>
        //[Authorize(Policy = Policies.IsAdmin)]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<GuideDTO>> CreateGuide([FromBody] Guide guide)
        {
            if (guide == null)
            {
                return BadRequest();
            }
            //if (guide)
            try
            {
                await _guideRepository.Create(guide);

                var guideRabbit = new GuideCreateEvent  
                {
                    Id = Guid.NewGuid(),
                    Create = DateTime.Now,
                    MessageType = 1,
                    Message = "�������� ����� ��� - " + guide.Name
                };
                await _publishEndpoint.Publish(guideRabbit);

                return CreatedAtAction(nameof(GetGuideById), new {id = guide.Id}, guide);
            }  
            catch (Exception ex)
            {
                _logger.LogError($"Error in CreateGuide : {ex.Message}");

                return BadRequest();
            }
        }

        // PUT api/guides/{id}
        /// <summary>
        /// Update a guide
        /// </summary>
        /// <param name="id"></param>
        //[Authorize(Policy = Policies.IsAdmin)]
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> UpdateGuide(int id, [FromBody] Guide guide)
        {

            if (guide == null)
            {
                return NotFound();
            }

            try
            {
                await _guideRepository.Update(guide);

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error in UpdateProduct : {ex.Message}");

                return BadRequest();
            }
        }

        // DELETE api/guides/{id}
        /// <summary>
        /// Delete a guide
        /// </summary>
        /// <param name="id"></param>
        //[Authorize(Policy = Policies.IsAdmin)]
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> DeleteGuide(int id)
        {
            var guide = await _guideRepository.GetByIdAsync(id);

            if (guide == null)
            {
                return NotFound();
            }

            try
            {
                await _guideRepository.Delete(guide);
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error in DeleteProduct : {ex.Message}");
                return BadRequest();
            }
        }
    }
}