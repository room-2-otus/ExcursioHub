using Microsoft.AspNetCore.Mvc;

namespace ExcursioHub.Server.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BaseApiController : ControllerBase
    {
    }
}