using ExcursioHub.Server.Contracts;
using ExcursioHub.Shared.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ExcursioHub.Server.Controllers
{
    public class CategoriesController : BaseApiController
    {
        private readonly ILogger<CategoriesController> _logger;
        private readonly ICategoryRepository _categoryRepository;

        public CategoriesController(ILogger<CategoriesController> logger, ICategoryRepository categoryRepository)
        {
            _logger = logger;
            _categoryRepository = categoryRepository;
        }

        // GET api/categories/lang/{languageId}
        [HttpGet("lang/{languageId}")]
        [ResponseCache(Duration = 300, VaryByQueryKeys = new[] {"languageId"})]
        public async Task<ActionResult<IEnumerable<Category>>> GetCategories(int languageId)
        {
            try
            {
                var categories = await _categoryRepository.ListAllAsync(languageId);

                return Ok(categories);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error in GetCategories : {ex.Message}");

                return StatusCode(500, "Internal server error");
            }
        }

        // GET api/categories/{id}
        [HttpGet("{id}")]
        [ResponseCache(Duration = 300, VaryByQueryKeys = new[] {"id"})]
        public async Task<ActionResult> GetCategoryById(int id)
        {
            try
            {
                var category = await _categoryRepository.GetByIdAsync(id);

                if (category == null)
                {
                    _logger.LogError($"Category with id: {id}, not found");

                    return NotFound();
                }
                else
                {
                    return Ok(category);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error in GetCategoryById : {ex.Message}");

                return StatusCode(500, "Internal server error");
            }
        }

        // POST api/categories
        /// <summary>
        /// Creates a new category
        /// </summary>
        ///// <returns>category</returns>
        //[Authorize(Policy = Policies.IsAdmin)]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<Category>> CreateCategory([FromBody] Category category)
        {
            if (category == null)
            {
                return BadRequest();
            }

            try
            {
                await _categoryRepository.Create(category);

                return CreatedAtAction(nameof(GetCategoryById), new { id = category.Id }, category);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error in CreateCategory : {ex.Message}");

                return BadRequest();
            }
        }

        // PUT api/categories/{id}
        /// <summary>
        /// Update a category
        /// </summary>
        /// <param name="id"></param>
        //[Authorize(Policy = Policies.IsAdmin)]
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> UpdateCategory(int id, [FromBody] Category category)
        {

            if (category == null)
            {
                return NotFound();
            }

            try
            {
                await _categoryRepository.Update(category);

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error in UpdateCategory : {ex.Message}");

                return BadRequest();
            }
        }

        // DELETE api/categories/{id}
        /// <summary>
        /// Delete a category
        /// </summary>
        /// <param name="id"></param>
        //[Authorize(Policy = Policies.IsAdmin)]
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> DeleteCategory(int id)
        {
            var category = await _categoryRepository.GetByIdAsync(id);

            if (category == null)
            {
                return NotFound();
            }

            try
            {
                await _categoryRepository.Delete(category);
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error in DeleteCategory : {ex.Message}");
                return BadRequest();
            }
        }

    }
}