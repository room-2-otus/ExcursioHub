﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ExcursioHub.Server.Data.Migrations
{
    public partial class GuidLangs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "LanguageId",
                table: "Guides",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LanguageId",
                table: "Guides");
        }
    }
}
