using ExcursioHub.Server.Contracts;
using ExcursioHub.Shared.Models.Orders;

namespace ExcursioHub.Server.Data
{
    public class OrderRepository : RepositoryBase<Order>, IOrderRepository
    {
        public OrderRepository(ApplicationDbContext repositoryContext) : base(repositoryContext)
        {
        }
    }
}