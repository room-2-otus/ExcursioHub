﻿using ExcursioHub.Server.Contracts;
using ExcursioHub.Shared.Models;
using Microsoft.EntityFrameworkCore;

namespace ExcursioHub.Server.Data
{
    public class LocalizedRepository<T> : RepositoryBase<T>, ILocalizedRepository<T> where T : LocalizedEntity
    {
        private readonly ApplicationDbContext _repositoryContext;

        public LocalizedRepository(ApplicationDbContext repositoryContext) : base(repositoryContext)
        {
            _repositoryContext=repositoryContext;
        }
        public async Task<IEnumerable<T>> ListAllAsync(int languageId)
        {
            if (languageId == 0)
            {
                return await _repositoryContext.Set<T>()
                   .AsNoTracking()
                   .ToListAsync();
            }
            return await _repositoryContext.Set<T>()
               .AsNoTracking()
               .Where(e => e.LanguageId == languageId)
               .ToListAsync();
        }
    }
}
