﻿using ExcursioHub.Server.Contracts;
using ExcursioHub.Shared.Models;

namespace ExcursioHub.Server.Data
{
    public class CategoryRepository : LocalizedRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(ApplicationDbContext repositoryContext) : base(repositoryContext)
        {
        }
    }
}
