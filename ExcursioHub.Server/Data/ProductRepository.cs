﻿using ExcursioHub.Server.Contracts;
using ExcursioHub.Shared.Models;

namespace ExcursioHub.Server.Data
{
    public class ProductRepository : LocalizedRepository<Product>, IProductRepository
    {
        public ProductRepository(ApplicationDbContext repositoryContext) : base(repositoryContext)
        {
        }
    }
}
