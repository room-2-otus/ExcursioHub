using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;
using ExcursioHub.Client.Pages.Admin;
using ExcursioHub.Shared.Models;
using Microsoft.EntityFrameworkCore;

namespace ExcursioHub.Server.Data
{
    public class SeedData
    {
        public static async Task SeedGuidesDataAsync(ApplicationDbContext context)
        {
            if (await context.Guides.AnyAsync()) return;

            var guidesData = await File.ReadAllTextAsync("./Data/SeedData/guides.json");
            var guides = JsonSerializer.Deserialize<List<Guide>>(guidesData);

            var strategy = context.Database.CreateExecutionStrategy();

            strategy.Execute(() =>
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    foreach (var guide in guides)
                    {
                        context.Guides.Add(guide);
                    }

                    context.Database.ExecuteSqlInterpolated($"SET IDENTITY_INSERT Guides ON;");
                    context.SaveChanges();
                    context.Database.ExecuteSqlInterpolated($"SET IDENTITY_INSERT Guides OFF");
                    transaction.Commit();
                }
            });
        }

        public static async Task SeedCategoriesDataAsync(ApplicationDbContext context)
        {
            if (await context.Categories.AnyAsync()) return;
            
            var categoriesData = await File.ReadAllTextAsync("./Data/SeedData/categories.json");
            var categories = JsonSerializer.Deserialize<List<Category>>(categoriesData);

            var strategy = context.Database.CreateExecutionStrategy();

            strategy.Execute(() =>
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    foreach (var category in categories)
                    {
                        context.Categories.Add(category);
                    }

                    context.Database.ExecuteSqlInterpolated($"SET IDENTITY_INSERT Categories ON;");
                    context.SaveChanges();
                    context.Database.ExecuteSqlInterpolated($"SET IDENTITY_INSERT Categories OFF");
                    transaction.Commit();
                }
            });
        }

        public static async Task SeedProductsDataAsync(ApplicationDbContext context)
        {
            if (await context.Products.AnyAsync()) return;

            var productsData = await File.ReadAllTextAsync("./Data/SeedData/products.json");
            var products = JsonSerializer.Deserialize<List<ExcursioHub.Shared.Models.Product>>(productsData);

            var strategy = context.Database.CreateExecutionStrategy();

            strategy.Execute(() =>
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    foreach (var product in products)
                    {
                        context.Products.Add(product);
                    }

                    context.Database.ExecuteSqlInterpolated($"SET IDENTITY_INSERT Products ON;");
                    context.SaveChanges();
                    context.Database.ExecuteSqlInterpolated($"SET IDENTITY_INSERT Products OFF");
                    transaction.Commit();
                }
            });
        }      
        
        public static async Task SeedLanguagesDataAsync(ApplicationDbContext context)
        {
            if (await context.Languages.AnyAsync()) return;

            var languagesData = await File.ReadAllTextAsync("./Data/SeedData/languages.json");
            var languages = JsonSerializer.Deserialize<List<Language>>(languagesData);

            var strategy = context.Database.CreateExecutionStrategy();

            strategy.Execute(() =>
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    foreach (var language in languages)
                    {
                        context.Languages.Add(language);
                    }

                    context.Database.ExecuteSqlInterpolated($"SET IDENTITY_INSERT Languages ON;");
                    context.SaveChanges();
                    context.Database.ExecuteSqlInterpolated($"SET IDENTITY_INSERT Languages OFF");
                    transaction.Commit();
                }
            });
        }


    }
}
