﻿using ExcursioHub.Server.Contracts;
using ExcursioHub.Shared.Models;
using Microsoft.EntityFrameworkCore;

namespace ExcursioHub.Server.Data
{
    public class LanguageRepository : RepositoryBase<Language>, ILanguageRepository
    {
        private readonly ApplicationDbContext _repositoryContext;
        public LanguageRepository(ApplicationDbContext repositoryContext) : base(repositoryContext) 
        {
            _repositoryContext = repositoryContext;
        }


        public async Task<Language> GetLanguageByLanguageIdAsync(string languageId)
        {
            return await _repositoryContext.Set<Language>()
                .AsNoTracking()
                .FirstOrDefaultAsync(e => e.LanguageId == languageId);
        }
    }
}
