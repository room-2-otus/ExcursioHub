﻿using ExcursioHub.Server.Contracts;
using ExcursioHub.Shared.Models;

namespace ExcursioHub.Server.Data
{
    public class GuideRepository : RepositoryBase<Guide>, IGuideRepository
    {
        public GuideRepository(ApplicationDbContext repositoryContext) : base(repositoryContext)
        {
        }
    }
}
