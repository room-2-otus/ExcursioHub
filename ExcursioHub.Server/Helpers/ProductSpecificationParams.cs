﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExcursioHub.Server.Helpers
{
    public class ProductSpecificationParams
    {
        private const int MaxPageSize = 24;
        public int PageIndex { get; set; } = 1;

        private int _pageSize = 8;

        public int PageSize
        {
            get => _pageSize;
            set => _pageSize = (value > MaxPageSize) ? MaxPageSize : value;
        }

        public int? LanguageId { get; set; }
        public int? GuideId { get; set; }
        public int? CategoryId { get; set; }
        public string Sort { get; set; }
        private string _search;
        public string Search
        {
            get => _search;
            set => _search = value.ToLower();
        }

        public bool IsAvailable { get; set; } = true;
        public bool IsAdmin { get; set; } = false;
    }
}
