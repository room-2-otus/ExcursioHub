using System.Collections.Generic;
using System.Threading.Tasks;
using ExcursioHub.Shared.DTOs;
using ExcursioHub.Shared.Models.Orders;

namespace ExcursioHub.Server.Contracts
{
    public interface IOrderService
    {
        Task<Order> CreateOrderAsync(string buyerEmail, OrderCreateDTO order);
        Task<IEnumerable<Order>> GetOrdersForUserAsync(string buyerEmail);
        Task<Order> GetOrderByIdAsync(int id, string buyerEmail);
    }
}