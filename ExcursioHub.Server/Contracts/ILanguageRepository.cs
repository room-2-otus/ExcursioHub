﻿using ExcursioHub.Shared.Models;

namespace ExcursioHub.Server.Contracts
{
    public interface ILanguageRepository : IRepositoryBase<Language>
    {
        Task<Language> GetLanguageByLanguageIdAsync(string languageId);
    }
}
