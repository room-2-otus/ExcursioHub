﻿using ExcursioHub.Shared.Models;

namespace ExcursioHub.Server.Contracts
{
    public interface ICategoryRepository : ILocalizedRepository<Category>
    {
    }
}
