﻿using ExcursioHub.Shared.Models;

namespace ExcursioHub.Server.Contracts
{
    public interface IProductRepository : ILocalizedRepository<Product>
    {
    }
}
