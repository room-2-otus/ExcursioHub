﻿using ExcursioHub.Shared.Models;

namespace ExcursioHub.Server.Contracts
{
    public interface ILocalizedRepository<T> : IRepositoryBase<T> where T : LocalizedEntity
    {
        Task<IEnumerable<T>> ListAllAsync(int languageId);
    }
}