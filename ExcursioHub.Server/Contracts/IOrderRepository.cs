using ExcursioHub.Shared.Models.Orders;

namespace ExcursioHub.Server.Contracts
{
    public interface IOrderRepository : IRepositoryBase<Order>
    {
    }
}