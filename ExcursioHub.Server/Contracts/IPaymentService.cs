using System.Threading.Tasks;
using ExcursioHub.Shared.DTOs;
using ExcursioHub.Shared.Entities;
using ExcursioHub.Shared.Models.Orders;

namespace ExcursioHub.Server.Contracts
{
    public interface IPaymentService
    {
        Task<PaymentIntentResult> CreateOrUpdatePaymentIntent(PaymentIntentCreateDTO paymentIntentCreateDTO);
        Task<Order> UpdateOrderPaymentStatus(string paymentIntentId, OrderStatus status);
    }
}