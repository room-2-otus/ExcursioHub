﻿using ExcursioHub.Shared.Models;

namespace ExcursioHub.Server.Contracts
{
    public interface IGuideRepository : IRepositoryBase<Guide>
    {
    }
}
