using ExcursioHub.Booking.Service.Data;
using ExcursioHub.BookingService;
using ExcursioHub.BookingService.Controllers;
using ExcursioHub.BookingService.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace ExcursioHub.Booking.Service.Tests
{
    public class TourReservationControllerTests
    {
        [Fact]
        public async Task ReserveTour_WithValidData_ReturnsOk()
        {
            var reservationRequest = new ReservationRequest
            {
                ProductId = 1,
                ClientId = 1,
                DateTime = DateTime.Now,
                NumberOfPeople = 2
            };

            var dbContext = new Mock<ApplicationDbContext>();
            var paymentServiceClient = new Mock<PaymentServiceClient>();
            var controller = new TourReservationController(dbContext.Object, paymentServiceClient.Object);
            var result = await controller.ReserveTour(reservationRequest);
            
            Assert.IsType<ObjectResult>(result);
        }

        [Fact]
        public async Task GetReservation_WithValidReservationId_ReturnsOk()
        {
            var reservationId = 1;
            var reservation = new Reservation { ReservationId = reservationId };

            var dbContext = new Mock<ApplicationDbContext>();
            dbContext.Setup(x => x.Reservations.FindAsync(reservationId)).ReturnsAsync(reservation);

            var paymentServiceClient = new Mock<PaymentServiceClient>();
            var controller = new TourReservationController(dbContext.Object, paymentServiceClient.Object);
            var result = await controller.GetReservation(reservationId);

            Assert.IsType<ObjectResult>(result);
        }

        /*[Fact]
        public async Task GetAllReservations_ReturnsOk()
        {
            var reservations = new List<Reservation>
            {
                new Reservation { ReservationId = 1 },
                new Reservation { ReservationId = 2 },
                new Reservation { ReservationId = 3 }
            };

            var dbContext = new Mock<ApplicationDbContext>();
            dbContext.Setup(x => x.Reservations.ToListAsync()).ReturnsAsync(reservations);
            //dbContext.Setup(x => x.Reservations.All()).ReturnsAsync(reservations);

            var paymentServiceClient = new Mock<PaymentServiceClient>();
            var controller = new TourReservationController(dbContext.Object, paymentServiceClient.Object);
            var result = await controller.GetAllReservations();

            Assert.IsType<ObjectResult>(result);
        }*/

        [Fact]
        public async Task UpdateReservationStatus_WithValidData_ReturnsOk()
        {
            var reservationId = 1;
            var newStatus = ReservationStatus.Confirmed;

            var reservation = new Reservation { ReservationId = reservationId };
            var dbContext = new Mock<ApplicationDbContext>();
            dbContext.Setup(x => x.Reservations.FindAsync(reservationId)).ReturnsAsync(reservation);

            var paymentServiceClient = new Mock<PaymentServiceClient>();
            var controller = new TourReservationController(dbContext.Object, paymentServiceClient.Object);
            var result = await controller.UpdateReservationStatus(reservationId, newStatus);

            Assert.IsType<OkObjectResult>(result);
        }
    }
}