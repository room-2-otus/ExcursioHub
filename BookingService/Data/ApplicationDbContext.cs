﻿using ExcursioHub.BookingService.Model;
using Microsoft.EntityFrameworkCore;

namespace ExcursioHub.Booking.Service.Data
{   public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options) 
        {
        }
        public ApplicationDbContext()
        {
        }
        //public DbSet<Reservation> Reservations { get; set; }
        public virtual DbSet<Reservation> Reservations { get; set; }
    }

    
}
