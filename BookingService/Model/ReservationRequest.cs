﻿namespace ExcursioHub.BookingService.Model
{
    public class ReservationRequest
    {
        public int ProductId { get; set; }
        public int ClientId { get; set; }
        public DateTime DateTime { get; set; }
        public int NumberOfPeople { get; set; }
    }
}