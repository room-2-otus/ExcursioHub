﻿namespace ExcursioHub.BookingService.Model
{
    public class Reservation
    {
        public int ReservationId { get; set; }
        public int ProductId { get; set; }
        public int ClientId { get; set; }
        public int NumberOfPeople { get; set; }
        public DateTime DateTime { get; set; }
        public ReservationStatus Status { get; set; }
    }
}