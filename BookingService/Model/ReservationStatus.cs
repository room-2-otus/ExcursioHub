﻿namespace ExcursioHub.BookingService.Model
{
    public enum ReservationStatus
    {
        Pending,
        Confirmed,
        Canceled
    }
}