﻿using ExcursioHub.Booking.Service.Data;
using ExcursioHub.BookingService.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ExcursioHub.BookingService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TourReservationController(ApplicationDbContext context, PaymentServiceClient paymentServiceClient) : ControllerBase
    {
        private readonly PaymentServiceClient _paymentServiceClient = paymentServiceClient;
        private readonly ApplicationDbContext _context = context;

        /// <summary>
        /// Метод для бронирования тура.
        /// </summary>
        /// <param name="reservationRequest">Запрос на бронирование тура.</param>
        /// <returns>Результат выполнения операции.</returns>
        /// POST api/TourReservation/ReserveTour
        [HttpPost("ReserveTour")]
        public async Task<ActionResult> ReserveTour([FromBody] ReservationRequest reservationRequest)
        {
            try
            {
                /*if (!CheckProductExists(reservationRequest.ProductId) || !CheckClientExists(reservationRequest.ClientId))
                {
                    return BadRequest("Invalid product or client.");
                }*/
                var reservation = new Reservation
                {
                    ReservationId = 0,//Guid.NewGuid()
                    ProductId = reservationRequest.ProductId,
                    ClientId = reservationRequest.ClientId,
                    DateTime = reservationRequest.DateTime,
                    NumberOfPeople = reservationRequest.NumberOfPeople,
                    Status = ReservationStatus.Pending // Новое бронирование имеет статус "В ожидании"
                };

                _context.Reservations.Add(reservation);
                await _context.SaveChangesAsync();
                _paymentServiceClient.InitiatePayment(reservation.ReservationId);

                return Ok(reservation);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal Server Error: {ex.Message}");
            }
        }

        /// <summary>
        /// Метод для получения информации о бронировании по его идентификатору.
        /// </summary>
        /// <param name="reservationId">Идентификатор бронирования.</param>
        /// <returns>Информация о бронировании.</returns>
        /// GET api/TourReservation/GetReservation/{reservationId}
        [HttpGet("GetReservation/{reservationId}")]
        public async Task<ActionResult> GetReservation(int reservationId)
        {
            try
            {
                var reservation = await _context.Reservations.FirstOrDefaultAsync(e => e.ReservationId == reservationId);
                if (reservation == null)
                {
                    return NotFound("Reservation not found.");
                }
                return Ok(reservation);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal Server Error: {ex.Message}");
            }
        }

        /// <summary>
        /// Метод для получения всех бронирований.
        /// </summary>
        /// <returns>Список всех бронирований.</returns>
        /// GET api/TourReservation/GetAllReservations
        [HttpGet("GetAllReservations")]
        public async Task<ActionResult> GetAllReservations()
        {
            try
            {
                var reservations = await _context.Reservations.ToListAsync();
                return Ok(reservations);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal Server Error: {ex.Message}");
            }
        }

        /// <summary>
        /// Метод для обновления статуса бронирования после успешной оплаты.
        /// </summary>
        /// <param name="reservationId">Идентификатор бронирования, которое требуется обновить.</param>
        /// <param name="newStatus">Новый статус бронирования.</param>
        /// <returns>Результат выполнения операции.</returns>
        [HttpPut("UpdateReservationStatus")]
        public async Task<ActionResult> UpdateReservationStatus(int reservationId, ReservationStatus newStatus)
        {
            try
            {
                var reservation = await _context.Reservations.FindAsync(reservationId);

                if (reservation == null)
                {
                    return NotFound("Reservation not found.");
                }

                reservation.Status = newStatus;
                await _context.SaveChangesAsync();

                return Ok("Reservation status updated successfully.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal Server Error: {ex.Message}");
            }
        }
        private bool CheckProductExists(int productId)
        {
            // тут запрос проверки существования продукта 
            throw new NotImplementedException();
        }
        private bool CheckClientExists(int clientId)
        {
            // тут запрос проверки существования клиента
            throw new NotImplementedException();
        }

    }
}
