# ExcursioHub

ExcursioHub is a state-of-the-art .NET excursion booking application that seamlessly integrates a Blazor WebAssembly frontend with a sophisticated Web API backend.

## Features

- **Frontend**: A cutting-edge hosted Blazor WebAssembly provides a dynamic user interface.
- **Backend**: Robust Web API leveraging Entity Framework and SQL Server for optimal data handling.
- **Checkout and Payment**: Complete checkout process with payment integration via Montonio API for secure financial transactions.
- **Security**: Implements .NET Core Identity and JWT bearer tokens for solid authentication and authorization.
- **Design Patterns**: Employs repository and specification patterns for code clarity and maintainability.
- **Performance**: Features API Response Caching to speed up performance.
- **Responsive Design**: Ensures a mobile-first responsive layout, with styling powered by Tailwind CSS.
- **Booking Functionality**: Employs Blazor's `CascadingParameters` for sophisticated and intuitive booking management.
- **Data Operations**: Supports pagination, sorting, and filtering for a seamless data interaction.
- **Validation**: Uses Fluent Validation for ensuring data integrity.

## Setup

### Prerequisites

- **.NET 6 SDK**: Ensure you have the latest version installed.
- **SQL Server**: Install locally or set up in a Docker container.
- **Visual Studio 2022 or higher**: Required if you're using Visual Studio.

- **Node.js**: Optional, only if you want to modify Tailwind CSS settings.
- **Docker**: Optional, only if you want to set up SQL Server in a Docker container.
- **Montonio API Key**: Optional, only if you want to test the payment functionality.

### Running with Docker

To set up a SQL Server using Docker, run:

docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=P@ssw0rd1234" -p 1433:1433 -d mcr.microsoft.com/mssql/server:2017-latest


Alternatively, use `docker-compose` in the root directory:

docker-compose up -d


### Initial Data Seeding with Admin Credentials

To manage the seeding of initial data using an admin role:

1. Open the `appsettings.Development.json` file.
2. Locate the `AdminAuthentication` section and enter the desired admin credentials.
3. These credentials will be used to create an admin user in the Identity section of the database.
4. Confirm that the application's environment is set to `Development` mode, as data seeding is configured to occur only in this environment.


### Running the Application

#### Setup Instructions for Visual Studio 2022

- Run Visual Studio 2022 as an administrator on the initial launch. This is required to enable the automatic creation of the database without any permission issues.
- Make sure the `ExcursioHub.Server` project is set as the startup project.
- To start the application, simply press `F5`.

### Database Configuration

Modify the connection strings in `appsettings.Development.json` within `ExcursioHub.Server` to align with your SQL Server settings.

### Image Assets

All images are hosted on imgbb.com and are available for public use.

## Contributing

Interested in contributing? Please open an issue to discuss your proposed changes before submitting a pull request.

## License

ExcursioHub is open-source software licensed under the [MIT License](https://choosealicense.com/licenses/mit/).


