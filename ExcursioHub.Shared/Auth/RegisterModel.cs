namespace ExcursioHub.Shared.Auth
{
    public class RegisterModel
    {
        // Fluent Validation provided by ExcursioHub.Shared.Validators.RegistrationValidator
        
        public string DisplayName { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }
    }
}