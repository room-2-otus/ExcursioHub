﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcursioHub.Shared.Models
{
    public class Language : BaseEntity
    {
        public string LanguageId { get; set; }
        public string LanguageName { get; set; }
    }
}
