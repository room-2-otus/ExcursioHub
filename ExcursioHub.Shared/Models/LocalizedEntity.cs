﻿using System.ComponentModel.DataAnnotations;

namespace ExcursioHub.Shared.Models
{
    public class LocalizedEntity: BaseEntity
    {
        [Required]
        public int LanguageId { get; set; }
    }
}
