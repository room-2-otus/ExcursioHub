namespace ExcursioHub.Shared.DTOs
{
    public class ProductDTO
    {
        public int Id { get; set; }
        
        public string Name { get; set; }

        public string Description { get; set; }

        public int ProductId { get; set; }

        public decimal Price { get; set; }

        public string ImageUrl { get; set; }

        public int GuideId { get; set; }

        public string Guide { get; set; }

        public int CategoryId { get; set; }

        public string Category { get; set; }
        
        public bool IsAvailable { get; set; }

        public decimal SalePrice { get; set; }
        public int LanguageId { get; set; }
    }
}