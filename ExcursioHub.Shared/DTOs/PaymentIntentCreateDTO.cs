using System.Collections.Generic;
using ExcursioHub.Shared.Entities;

namespace ExcursioHub.Shared.DTOs
{
    public class PaymentIntentCreateDTO
    {
        public List<BasketItem> Items { get; set; }
        public string PaymentIntentId { get; set; }
    }
}