using System;
using System.Collections.Generic;
using ExcursioHub.Shared.Models.Orders;

namespace ExcursioHub.Shared.DTOs
{
    public class OrderDetailsDTO
    {
        public int Id { get; set; }
        public string BuyerEmail { get; set; }
        public DateTimeOffset OrderDate { get; set; }
        public Address SendToAddress { get; set; }
        public List<OrderItemDTO> OrderItems { get; set; }
        public decimal Total { get; set; }
        public string Status { get; set; }
    }
}