﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcursioHub.Shared.DTOs
{
    public class GuideDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int LanguageId { get; set; }
    }
}
