using System.ComponentModel.DataAnnotations;

namespace ExcursioHub.Shared.DTOs
{
    public class ProductCreateDTO
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public decimal Price { get; set; }

        [Required]
        public string ImageUrl { get; set; }

        [Required]
        public int GuideId { get; set; }

        [Required]
        public int CategoryId { get; set; }

        [Required]
        public int LanguageId { get; set; }

        [Required]
        public decimal SalePrice
        {
            get => Price;
        }        
    }
}