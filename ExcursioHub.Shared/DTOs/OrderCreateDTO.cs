using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ExcursioHub.Shared.Entities;
using ExcursioHub.Shared.Models.Orders;

namespace ExcursioHub.Shared.DTOs
{
    public class OrderCreateDTO
    {
        [Required]
        public List<BasketItem> Items { get; set; }
        
        [Required]
        public Address SendToAddress { get; set; }
        
        [Required]
        public string PaymentIntentId { get; set; }
    }
}