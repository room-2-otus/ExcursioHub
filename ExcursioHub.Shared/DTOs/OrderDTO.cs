using ExcursioHub.Shared.Models.Orders;

namespace ExcursioHub.Shared.DTOs
{
    public class OrderDTO
    {
        public string OrderId { get; set; }
        public Address SendToAddress { get; set; }
    }
}