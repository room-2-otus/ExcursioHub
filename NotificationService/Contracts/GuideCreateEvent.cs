﻿namespace Contracts
{
    public record GuideCreateEvent
    {
        public Guid Id { get; set; }

        public DateTime Create { get; set; }

        public int MessageType { get; set; }

        public string Message { get; set; }
    }
}