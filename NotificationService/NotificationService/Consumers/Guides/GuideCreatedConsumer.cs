﻿using Contracts;
using MassTransit;
using System.Net.Mail;
using System.Net;
using Newtonsoft.Json;

namespace NotificationService.Consumers.Guides
{
    public class GuideCreatedConsumer : IConsumer<GuideCreateEvent>
    {
        public class ClientsRequest
        {
            public Data data { get; set; }
        }

        public class Data
        {
            public Clientsinfo[] clientsInfo { get; set; }
        }

        public class Clientsinfo
        {
            public int Id { get; set; }

            public string FirstName { get; set; }

            public string SecondName { get; set; }

            public string LastName { get; set; }

            public string Email { get; set; }

            public string PhoneNumber { get; set; }

            public DateTime RegDate { get; set; }
        }

        public async Task Consume(ConsumeContext<GuideCreateEvent> context)
        {
            var mainMail = "excursiohub@gmail.com";
            var pass = "ltaj orhd ahoy ocfg";
            var client = new SmtpClient("smtp.gmail.com", 587)
            {
                EnableSsl = true,
                Credentials = new NetworkCredential(mainMail, pass)
            };
            var mail = new MailMessage();
            var allMails = GetAllMails();
            foreach (var a in allMails.data.clientsInfo)
            {
                mail.From = new MailAddress(mainMail, "Test");
                mail.To.Add(a.Email);
                mail.Subject = "Test";
                mail.Body = context.Message.Message;
                mail.IsBodyHtml = true;
                await client.SendMailAsync(mail);
            }
        }


        ClientsRequest GetAllMails()
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.PostAsJsonAsync("https://localhost:7089/graphql/", new { query = "query { clientsInfo { email } }" }).Result;
            var all = response.Content.ReadAsStringAsync();
            var clientsInfo = JsonConvert.DeserializeObject<ClientsRequest>(all.Result.ToString());
            return clientsInfo;
        }
    }
}
