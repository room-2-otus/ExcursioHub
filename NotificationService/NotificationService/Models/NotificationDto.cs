﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotificationService.Models
{
    public class NotificationDto
    {
        public Guid Id { get; set; }
        public string Body { get; set; }
        public DateTime SendingDateTime { get; set; }
        public Guid UserID { get; set; }
        public string Address { get; set; }
    }
}
