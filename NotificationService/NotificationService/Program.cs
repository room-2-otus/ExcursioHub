using Contracts;
using MassTransit;
using NotificationService.Consumers;
using NotificationService.Consumers.Guides;

var builder = WebApplication.CreateBuilder(args);
var r = builder.Configuration["NotificationMessage:Host"];


// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddMassTransit(conf =>
{
    conf.SetKebabCaseEndpointNameFormatter();

    conf.AddConsumer<GuideCreatedConsumer>();

    conf.UsingRabbitMq((context, config) =>
    {

        config.Host(
            "localhost",
            "/",
            x =>
            {
                x.Username(builder.Configuration["NotificationMessage:Username"]);
                x.Password(builder.Configuration["NotificationMessage:Password"]);
            });
        config.ConfigureEndpoints(context);
    });
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
