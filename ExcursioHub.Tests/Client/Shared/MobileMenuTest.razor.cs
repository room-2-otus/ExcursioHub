﻿using Microsoft.Extensions.DependencyInjection;

namespace ExcursioHub.Client.Shared
{
    public partial class MobileMenuTest
    {
        public MobileMenuTest()
        {
            Services.AddLocalization(opts => { opts.ResourcesPath = "Resources"; });
        }
    }
}
