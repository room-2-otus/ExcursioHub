﻿using Microsoft.Extensions.DependencyInjection;

namespace ExcursioHub.Client.Shared
{
    public partial class NavMenuTest
    {
        public NavMenuTest()
        {
            Services.AddLocalization(opts => { opts.ResourcesPath = "Resources"; });
        }
    }
}
