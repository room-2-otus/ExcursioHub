﻿using Bunit;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using System.Globalization;

namespace ExcursioHub.Client.Components
{
    public partial class ClickSearchTest : TestContext
    {
        public ClickSearchTest()
        {
            Services.AddLocalization(opts => { opts.ResourcesPath = "Resources"; });

            //// Mock IOptions manually if needed
            //Services.AddSingleton<IOptions<LocalizationOptions>>(new OptionsWrapper<LocalizationOptions>(
            //    new LocalizationOptions { ResourcesPath = "Resources" }));

            //// If you have custom resources, ensure the culture is set correctly
            //CultureInfo.CurrentUICulture = new CultureInfo("en-US");
        }
    }
}
